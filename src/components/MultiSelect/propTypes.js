import { PropTypes } from 'react'

export default {
	caseSensitivity: PropTypes.bool,
	onChange: PropTypes.func,
	value: PropTypes.array,
	valueKey: PropTypes.string,
	labelKey: PropTypes.string
}
