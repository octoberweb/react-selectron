import classNames from '../../classNames.js'

export default {
	caseSensitivity: true,
	value: [],
	classNames,
	valueKey: 'value',
	labelKey: 'label'
}
