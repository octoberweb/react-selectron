'use strict';

Object.defineProperty(exports, "__esModule", {
	value: true
});
var classNames = {
	'multiselect': 'multiselect',
	'multiselect__selectedOptions': 'multiselect__selectedOptions',
	'multiselect__options': 'multiselect__options',
	'multiselect__option': 'multiselect__option',
	'multiselect__input': 'multiselect__input',
	'multiselect__loader': 'multiselect__loader',
	'input': 'input',
	'input__placeholder': 'input__placeholder',
	'options': 'options',
	'options__empty': 'options__empty',
	'options__loading': 'options__loading',
	'option': 'option',
	'option--focused': 'option--focused',
	'selectedOption': 'selectedOption',
	'selectedOption--focused': 'selectedOption--focused',
	'selectedOption__caption': 'selectedOption__caption',
	'selectedOption__remove': 'selectedOption__remove'
};

exports.default = classNames;